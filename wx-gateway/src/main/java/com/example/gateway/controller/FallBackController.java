package com.example.gateway.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mazeyi on 2019/12/6.
 */
@RestController
public class FallBackController {
    /**
     * gateway层面的hytrix降级处理，仅在路由转发地址无法访问的时候的执行，路由目的地发生内部错误需要其内部做相应的降级处理方案
     * @return
     */
    @GetMapping("/fallback")
    public String fallback() {
        Map<String, Object> result = new HashMap<>();
        result.put("code","100");
        result.put("message","服务暂时不可用");
        return JSONObject.toJSONString(result);
    }
}