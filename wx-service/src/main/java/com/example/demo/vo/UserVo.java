package com.example.demo.vo;

import lombok.Data;

/**
 * Created by mazeyi on 2020/1/7.
 */
@Data
public class UserVo {
    private long id;

    private String name;

    private Integer age;

}