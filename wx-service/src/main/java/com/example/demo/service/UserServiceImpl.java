package com.example.demo.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.example.demo.api.UserService;

/**
 * Created by mazeyi on 2020/1/6.
 */
@Service(version = "${demo.service.version}")
public class UserServiceImpl implements UserService {
    @Override
    public String say(String str) {
        return "hello world";
    }
}