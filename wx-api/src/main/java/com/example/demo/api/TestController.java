package com.example.demo.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mazeyi on 2019/12/6.
 */

@RestController
@RequestMapping("/wx-api")
public class TestController {

    @RequestMapping("/hello")
    public String test(String name){
        try{
            return "hello " + name;
        }catch (Exception e){
            return "系统内部错误";
        }
    }
}