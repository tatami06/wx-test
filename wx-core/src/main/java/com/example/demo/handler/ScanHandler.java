package com.example.demo.handler;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;
import com.example.demo.builder.TextBuilder;
import java.util.Map;

/**
 * Created by mazeyi on 2019/11/12.
 */
@Component
public class ScanHandler extends AbstractHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> map, WxMpService wxMpService, WxSessionManager wxSessionManager) throws WxErrorException {

        if(wxMessage.getEvent().equals(WxConsts.EventType.SCAN)){
            logger.debug("扫描事件");
        }
        return new TextBuilder().build("通过扫描二维码进入",wxMessage,wxMpService);


    }
}
