package com.example.demo.handler;

import com.example.demo.builder.ImageBuilder;
import com.example.demo.builder.TextBuilder;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.bean.result.WxMediaUploadResult;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mazeyi on 2019/11/12.
 */
@Component
public class MsgHandler extends AbstractHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> map, WxMpService wxMpService, WxSessionManager wxSessionManager) throws WxErrorException {
        if(wxMessage.getMsgType().equals(WxConsts.XmlMsgType.EVENT)){
            //TODO 可以选择将消息保存到本地
            logger.debug("event + " +wxMessage.getMsgType());
        }
        if(wxMessage.getMsgType().equals(WxConsts.XmlMsgType.TEXT)){
            if(wxMessage.getContent().equals("create")){
                WxMenu wxMenu = new WxMenu();
                List<WxMenuButton> list = new ArrayList<>();
                WxMenuButton wxMenuButton = new WxMenuButton();
                wxMenuButton.setName("test");
                wxMenuButton.setType("click");
                wxMenuButton.setKey("rselfmenu_0_0");
                list.add(wxMenuButton);
                wxMenu.setButtons(list);
                wxMpService.getMenuService().menuCreate(wxMenu);
            }

            return new TextBuilder().build("文本信息", wxMessage, wxMpService);


            // 初始化菜单

        }else if(wxMessage.getMsgType().equals(WxConsts.XmlMsgType.IMAGE)){
            // 生成临时二维码
            WxMpQrCodeTicket ticket = wxMpService.getQrcodeService().qrCodeCreateTmpTicket("test", 604800);
            File file = wxMpService.getQrcodeService().qrCodePicture(ticket);
            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            WxMediaUploadResult res;
            res = wxMpService.getMaterialService().mediaUpload("image", file);
            return new ImageBuilder().build(res.getMediaId(),wxMessage, wxMpService);

        }else if(wxMessage.getMsgType().equals(WxConsts.XmlMsgType.VIDEO)){
            return new TextBuilder().build("短视频", wxMessage, wxMpService);
        }else if(wxMessage.getMsgType().equals(WxConsts.XmlMsgType.MUSIC)){
            return new TextBuilder().build("音乐", wxMessage, wxMpService);
        }
        //当用户输入关键词如“你好”，“客服”等，并且有客服在线时，把消息转发给在线客服
        try {
            if (StringUtils.startsWithAny(wxMessage.getContent(), "你好", "客服")
                    && wxMpService.getKefuService().kfOnlineList()
                    .getKfOnlineList().size() > 0) {
                return WxMpXmlOutMessage.TRANSFER_CUSTOMER_SERVICE()
                        .fromUser(wxMessage.getToUser())
                        .toUser(wxMessage.getFromUser()).build();
            }
        } catch (WxErrorException e) {
            logger.error("客服系统未安装，请通知管理员");
           // e.printStackTrace();
        }

        //TODO 组装回复消息
       // String content = "收到信息内容：" + JsonUtils.toJson(wxMessage);
        String content = "hello world";
        return new TextBuilder().build(content, wxMessage, wxMpService);

    }
}
