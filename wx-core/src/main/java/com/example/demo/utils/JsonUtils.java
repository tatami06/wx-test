package com.example.demo.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by mazeyi on 2019/11/12.
 */
public class JsonUtils {
    public static String toJson(Object obj) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(obj);
    }
}
