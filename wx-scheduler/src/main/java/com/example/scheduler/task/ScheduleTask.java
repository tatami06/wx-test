package com.example.scheduler.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by mazeyi on 2019/12/6.
 */
@Slf4j
@Component
public class ScheduleTask {

    @Scheduled(cron = "*/1 * * * * ?")
    public void execute() {
        log.info("print word.");
        log.info(String.valueOf(System.currentTimeMillis()));
    }

}