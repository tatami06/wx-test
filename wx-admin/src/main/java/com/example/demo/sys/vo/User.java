package com.example.demo.sys.vo;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.sys.util.ReflectionUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by mazeyi on 2019/12/6.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private long id;

    private String name;

    private Integer age;

    private byte sex;

    @Override
    public String toString(){
        return ReflectionUtil.iteratorField(this);
    }



    public static void main(String[] args) {
        Integer i = -3;
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        System.out.println(uuid.hashCode() & Integer.MAX_VALUE);
        System.out.println("mario".hashCode() & Integer.MAX_VALUE);
        System.out.println(i.hashCode());
        System.out.println(Integer.MAX_VALUE);
        System.out.println(i.hashCode() & Integer.MAX_VALUE);
    }
}