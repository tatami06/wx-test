package com.example.demo.sys.service;

import com.example.demo.sys.vo.User;

import java.util.List;

/**
 * Created by mazeyi on 2019/12/6.
 */
public interface UserService {
    /**
     * 添加用户/修改用户
     *
     * @param user
     */
    void save(User user);

    /**
     * 删除用户
     * @param id
     */
    void deleteById(long id);

    /**
     * 查询所有
     * @return
     */
    List<User> findAll();

}