package com.example.demo.sys.service.impl;

import com.example.demo.sys.dao.UserRepository;
import com.example.demo.sys.service.UserService;
import com.example.demo.sys.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by mazeyi on 2019/12/6.
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void save(User user) {
        userRepository.save(user);
        log.info("admin 添加用户接口:{]", user.toString());
    }


    @Override
    public void deleteById(long id) {
        userRepository.deleteById(id);
        log.info("删除用户操作 用户id:{}",id);
    }

    @Override
    public List<User> findAll() {
        log.info("进行用户查询操作:{}");
        return userRepository.findAll();
    }
}