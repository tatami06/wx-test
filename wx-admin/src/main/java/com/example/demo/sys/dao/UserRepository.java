package com.example.demo.sys.dao;

import com.example.demo.sys.vo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * Created by mazeyi on 2019/12/6.
 */
@Component
public interface UserRepository extends JpaRepository<User, Long>{

}